package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.shipilov.tm.dto.response.ApplicationVersionResponse;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String DESCRIPTION = "Show version info.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull ApplicationVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
