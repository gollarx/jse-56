package ru.t1.shipilov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.TaskCreateRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-create";

    @NotNull
    private final String DESCRIPTION = "Create new task.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        taskEndpoint.createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
