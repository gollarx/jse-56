package ru.t1.shipilov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.ProjectStartByIndexRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIndexCommand extends AbstractProjectCommand{

    @NotNull
    private final String NAME = "project-start-by-index";

    @NotNull
    private final String DESCRIPTION = "Start project by index.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        request.setIndex(index);
        projectEndpoint.startProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
