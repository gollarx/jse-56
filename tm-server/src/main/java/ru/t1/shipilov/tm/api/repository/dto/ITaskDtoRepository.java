package ru.t1.shipilov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}

