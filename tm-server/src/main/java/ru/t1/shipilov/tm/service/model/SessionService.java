package ru.t1.shipilov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.shipilov.tm.api.repository.model.ISessionRepository;
import ru.t1.shipilov.tm.api.service.model.ISessionService;
import ru.t1.shipilov.tm.exception.entity.field.IdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.IndexIncorrectException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.exception.user.AuthenticationException;
import ru.t1.shipilov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull List<Session> result;
        try {
            result = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        boolean result;
        try {
            result = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable Session result;
        try {
            result = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable Session result;
        try {
            result = repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable Session session;
        try {
            session = repository.findOneByIndex(userId, index);
            if (session == null) return;
            entityManager.getTransaction().begin();
            repository.remove(userId, session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull Session session) {
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Session session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) {
        if (session.getUser() == null) throw new UserIdEmptyException();
        if (session.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(session.getUser().getId(), session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}
